package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/alexflint/go-arg"
)

/*
	Twirp uses a weird URL path convention, witch is about to change in
	the nearest release.

	But anyway, twirp is a codegeneration tool, not a framework, so you are free
	to codegen over codegen files :). All we want is a very easy change:

	"/twirp/aspc.ru_fe.SignService/SignEmailUp"  ->  "/api/SignService/SignEmailUp"
	and
	"ru_feProfile" -> "Profile"

 */

func main() {
	var args struct {
		Package string   `arg:"positional,required"`
		Globs   []string `arg:"positional,required"`
	}
	arg.MustParse(&args)

	// Get all files to make changes to
	targets := buildFileList(args.Globs)
	if len(targets) == 0 {
		failWithError(errors.New("no targets found"))
	}

	// Make changes
	for _, target := range targets {
		fmt.Println("\t", target)
		processFile(args.Package, target)
	}
}

func buildFileList(globs []string) []string {
	var targets []string
	// For every passed glob
	for _, glob := range globs {
		// get file matches
		matches, err := filepath.Glob(glob)
		if err != nil {
			failWithError(err)
		}

		// and add it to targets set, except directories
		for _, match := range matches {
			info, err := os.Stat(match)
			if err != nil {
				failWithError(err)
			}
			if info.IsDir() {
				continue
			}

			targets = append(targets, match)
		}
	}

	return targets
}

var urlPathRegexp = regexp.MustCompile(`"/twirp/.+"`)

func processFile(pack, path string) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		failWithError(err)
	}

	content := string(data)

	// Very inefficient, but that's a cli dev tool after all
	var stringsToChange = map[string]string{}
	for _, found := range urlPathRegexp.FindAllString(content, -1) {
		if _, ok := stringsToChange[found]; ok {
			continue
		}

		replace := found[strings.LastIndex(found, ".")+1:]
		replace = `"/api/` + replace
		fmt.Println("\t\t", found, " -> ", replace)

		stringsToChange[found] = replace
	}

	for orig, repl := range stringsToChange {
		content = strings.Replace(content, orig, repl, -1)
	}

	for _, packPart := range strings.Split(pack, ".") {
		content = regexp.MustCompile(`(` + packPart + `)([A-Z])`).ReplaceAllString(content, "$2")
	}

	if err = ioutil.WriteFile(path, []byte(content), 0); err != nil {
		failWithError(err)
	}
}

func failWithError(err error) {
	fmt.Println("Error: ", err.Error())
	fail()
}
func fail() {
	os.Exit(-1)
}
