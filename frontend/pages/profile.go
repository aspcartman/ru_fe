package pages

import (
	"html/template"
	"net/http"

	"bitbucket.org/aspcartman/ru_fe/api"
	"github.com/tiaotiao/mapstruct"
)

type ProfilePage struct {
	Templates      *template.Template
	ProfileService api.UserProfileService
}

func (p ProfilePage) Render(rw http.ResponseWriter, r *http.Request) {
	profile, err := p.ProfileService.GetUserProfile(r.Context(), &api.GetUserProfileArgs{
		UserId: "",
	})
	if err != nil {
		http.Error(rw, err.Error(), http.StatusForbidden)
		return
	}

	p.Templates.Lookup("profile").Execute(rw, mapstruct.Struct2Map(profile))
}

func (p ProfilePage) HandleAction(rw http.ResponseWriter, r *http.Request) {
	p.Templates.Lookup("profile").Execute(rw, nil)
}
