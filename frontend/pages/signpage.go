package pages

import (
	"html/template"
	"net/http"
	"strings"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/common/tokenutil"
	"github.com/sirupsen/logrus"
)

type SignPage struct {
	Templates    *template.Template
	SignService  api.SignService
	TokenService api.TokenService
}

func (p SignPage) Render(rw http.ResponseWriter, r *http.Request) {
	if err := p.Templates.Lookup("sign").Execute(rw, nil); err != nil {
		logrus.WithError(err).Error("failed rendering page")
	}
}

func (p SignPage) HandleAction(rw http.ResponseWriter, r *http.Request) {
	var tok *api.Token
	var err error

	switch {
	case strings.HasSuffix(r.RequestURI, "in"):
		creds := api.EmailCredentials{Email: r.PostFormValue("email"), Password: r.PostFormValue("password")}
		tok, err = p.SignService.SignEmailIn(r.Context(), &creds)
	case strings.HasSuffix(r.RequestURI, "up"):
		creds := api.EmailCredentials{Email: r.PostFormValue("email"), Password: r.PostFormValue("password")}
		tok, err = p.SignService.SignEmailUp(r.Context(), &creds)
	case strings.HasSuffix(r.RequestURI, "google"):
		token := api.GoogleCredentials{GoogleToken: r.PostFormValue("idtoken")}
		tok, err = p.SignService.SignGoogle(r.Context(), &token)
	case strings.HasSuffix(r.RequestURI, "out"):
		tok := api.Token{Token: getToken(r)}
		_, err = p.TokenService.InvalidateToken(r.Context(), &tok)
	default:
		http.Error(rw, "unknown action", http.StatusNotFound)
		return
	}

	if err != nil {
		http.Error(rw, err.Error(), http.StatusForbidden)
		logrus.WithError(err).Error("login failed")
		return
	}

	if tok != nil {
		tokenutil.PutTokenInResponse(rw, tok)
	} else {
		tokenutil.PutTokenInResponse(rw, &api.Token{})
	}

	http.Redirect(rw, r, "/", http.StatusFound)
}

func getToken(r *http.Request) string {
	c, err := r.Cookie("auth_token")
	if err != nil && c == nil {
		return ""
	}
	return c.Value
}
