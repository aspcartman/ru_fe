package pages

import (
	"html/template"
	"net/http"
)

type ProfileEditPage struct {
	Templates *template.Template
}

func (p ProfileEditPage) Render(rw http.ResponseWriter, r *http.Request) {
	p.Templates.Lookup("profile").Execute(rw, nil)
}

func (p ProfileEditPage) HandleAction(rw http.ResponseWriter, r *http.Request) {
	p.Templates.Lookup("profile").Execute(rw, nil)
}
