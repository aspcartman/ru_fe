package pages

import "net/http"

type IndexPage struct {
}

func (p IndexPage) Render(rw http.ResponseWriter, r *http.Request) {
	http.Redirect(rw, r, "/profile", http.StatusFound)
}

func (p IndexPage) HandleAction(rw http.ResponseWriter, r *http.Request) {
	//http.Redirect(rw, r, "/profile", http.StatusTemporaryRedirect)
}
