package pages

import (
	"net/http"
)

type Page interface {
	Render(rw http.ResponseWriter, r *http.Request)
	HandleAction(rw http.ResponseWriter, r *http.Request)
}

func PageHandler(p Page) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			p.Render(rw, r)
		case http.MethodPost:
			p.HandleAction(rw, r)
		}
	})
}
