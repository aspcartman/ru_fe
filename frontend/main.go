package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/common/tokenutil"
	"bitbucket.org/aspcartman/ru_fe/frontend/pages"
	"github.com/alexflint/go-arg"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
)

func main() {
	var args struct {
		BackendPath string `arg:"env:RUFE_BACKEND_PATH,required"`
	}
	arg.MustParse(&args)
	ts := getTemplatesFromDir("./frontend/templates")

	r := mux.NewRouter()
	r.Use(loggingMiddleware)
	authMid := tokenutil.RequestAuth{NoValidate: true}.AuthorizeRequestMiddleware

	r.PathPrefix("/sign").Handler(
		pages.PageHandler(
			pages.SignPage{
				Templates:    ts,
				SignService:  api.NewSignServiceProtobufClient(args.BackendPath, http.DefaultClient),
				TokenService: api.NewTokenServiceProtobufClient(args.BackendPath, http.DefaultClient),
			},
		),
	)

	r.PathPrefix("/profile").Handler(
		authMid(
			pages.PageHandler(
				pages.ProfilePage{
					Templates:      ts,
					ProfileService: api.NewUserProfileServiceProtobufClient(args.BackendPath, http.DefaultClient),
				},
			),
		),
	)

	r.PathPrefix("/").Handler(pages.PageHandler(pages.IndexPage{}))

	r.NotFoundHandler = loggingMiddleware(http.NotFoundHandler())

	fmt.Println("Frontend Started")
	http.ListenAndServe("0.0.0.0:8080", r)
}

func getTemplatesFromDir(dir string) *template.Template {
	templateSuffix := ".gohtml"
	ts := template.New("templates")

	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if info != nil && !info.IsDir() && strings.HasSuffix(info.Name(), templateSuffix) {
			tmplName, err := filepath.Rel(dir, path)
			if err != nil {
				return err
			}
			tmplName = tmplName[:len(tmplName)-len(templateSuffix)]

			content, err := ioutil.ReadFile(path)
			if err != nil {
				return err
			}

			_, err = ts.New(tmplName).Parse(string(content))
			if err != nil {
				return err
			}

			fmt.Println("Loaded template ", tmplName)
		}
		return nil
	})

	if err != nil {
		panic(err)
	}

	return ts
}

func loggingMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		t := time.Now()
		handler.ServeHTTP(rw, r)
		logrus.WithFields(logrus.Fields{"path": r.RequestURI, "time": time.Since(t)}).Info("incoming request")
	})
}
