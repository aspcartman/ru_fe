package tokenutil

import (
	"context"
	"net/http"

	"bitbucket.org/aspcartman/ru_fe/api"
)

type RequestAuth struct {
	TokenService api.TokenService
	NoValidate   bool
}

func (m RequestAuth) AuthorizeRequestMiddleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		// Get the token from authorization http header or from cookies
		token, userID, err := GetTokenFromRequest(r)
		if err != nil {
			http.Error(rw, "token validation failed: "+err.Error(), http.StatusUnauthorized)
			return
		}

		if !m.NoValidate {
			// Validate the token
			val, err := m.TokenService.ValidateToken(context.Background(), token)
			switch {
			case err != nil:
				http.Error(rw, "token validation failed: "+err.Error(), http.StatusUnauthorized)
				return
			case !val.Valid:
				http.Error(rw, "token validation failed: "+val.InvalidyReason, http.StatusUnauthorized)
				return
			}
			userID = val.UserId
		}

		r = r.WithContext(ContextWithToken(r.Context(), token, userID))
		handler.ServeHTTP(rw, r)
	})
}

