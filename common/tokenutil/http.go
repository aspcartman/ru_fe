package tokenutil

import (
	"errors"
	"net/http"
	"strings"

	"bitbucket.org/aspcartman/ru_fe/api"
	"github.com/gbrlsnchs/jwt"
)

func GetTokenFromRequest(r *http.Request) (*api.Token, string, error) {
	var token string

	// Try to get token from auth header and cookie
	hed := r.Header.Get("Authorization")
	cok, _ := r.Cookie("auth_token")
	switch {
	case hed != "" && strings.IndexByte(hed, ' ') > 0:
		token = hed[strings.IndexByte(hed, ' ')+1:]
	case cok != nil && cok.Value != "":
		token = cok.Value
	default:
		return nil, "", errors.New("no token neither in authorization header nor cookie")
	}

	jtok, err := jwt.FromString(token)
	if err != nil {
		return nil, "", err
	}

	userID := jtok.Audience()
	if userID == "" {
		return nil, "", errors.New("no user_id in token")
	}

	return &api.Token{Token: token}, userID, nil
}

func PutTokenInResponse(rw http.ResponseWriter, token *api.Token) {
	cook := http.Cookie{
		Name:     "auth_token",
		Value:    token.Token,
		Path:     "/",
		HttpOnly: true,
		//Expires: time.Now().Add(100 * time.Hour),
	}
	rw.Header().Set("Set-Cookie", cook.String())
}
