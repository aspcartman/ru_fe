package tokenutil

import (
	"context"
	"net/http"

	"bitbucket.org/aspcartman/ru_fe/api"
	"github.com/twitchtv/twirp"
)

func ContextWithToken(ctx context.Context, tok *api.Token, userID string) context.Context {
	ctx = context.WithValue(ctx, "userID", userID)
	ctx = context.WithValue(ctx, "auth_token", tok)

	// Side effect: set the token to a special key so that
	// it automatically get set into the following api calls
	ctx, _ = twirp.WithHTTPRequestHeaders(ctx, http.Header{
		"Authorization": []string{"Bearer " + tok.Token},
	})

	return ctx
}

func GetTokenFromContext(ctx context.Context) (*api.Token, string) {
	tok, _ := ctx.Value("auth_token").(*api.Token)
	userID, _ := ctx.Value("userID").(string)

	return tok, userID
}
