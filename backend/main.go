package main

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/controllers"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
	"bitbucket.org/aspcartman/ru_fe/common/tokenutil"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"github.com/twitchtv/twirp"
)

func main() {
	store := &storage.InMemStore{}

	// Token service used by other services.
	tokService := controllers.TokenController{
		Store: store,
	}

	// Authorization middleware
	authMid := tokenutil.RequestAuth{TokenService: tokService}.AuthorizeRequestMiddleware

	router := mux.NewRouter()
	router.Use(loggingMidleware)
	hooks := twirp.ServerHooks{Error: errorHook}

	router.PathPrefix(api.SignServicePathPrefix).Handler(api.NewSignServiceServer(controllers.AuthController{
		CredentialsStore: store,
		TokenService:     tokService,
	}, &hooks))

	router.PathPrefix(api.UserProfileServicePathPrefix).Handler(
		authMid(
			api.NewUserProfileServiceServer(controllers.ProfileController{
				Store:        store,
				TokenService: tokService,
			}, &hooks),
		),
	)

	router.NotFoundHandler = loggingMidleware(http.NotFoundHandler())

	fmt.Println("Started")
	http.ListenAndServe("0.0.0.0:8081", router)
}

func loggingMidleware(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		t := time.Now()
		handler.ServeHTTP(rw, r)
		logrus.WithFields(logrus.Fields{"path": r.RequestURI, "time": time.Since(t)}).Info("incoming request")
	})
}

func errorHook(ctx context.Context, err twirp.Error) context.Context {
	logrus.WithError(err).Error("rpc response with error")
	return ctx
}
