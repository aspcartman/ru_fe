package utility

import "testing"

func TestGMailMailer_SendEmail(t *testing.T) {
	gm := GMailMailer{"fakemailforjunks@gmail.com", "igromania123"} // Yep, that's junk email I don't care security of.
	err := gm.SendEmail("aspcartman@gmail.com", "test", []byte("Hello World"))
	if err != nil {
		t.Error(err)
	}
}
