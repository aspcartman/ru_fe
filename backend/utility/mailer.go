package utility

import "net/smtp"

/*
	A simple email sending interface
 */
type Mailer interface {
	SendEmail(to, subject string, body []byte) error
}

/*
	GMailMailer is a simple GoogleMail mail sender that
	uses insecure plain authorization.
 */
type GMailMailer struct {
	Login    string
	Password string
}

func (m GMailMailer) SendEmail(to, subject string, body []byte) error {
	auth := smtp.PlainAuth("", m.Login, m.Password, "smtp.gmail.com")
	return smtp.SendMail("smtp.gmail.com:587", auth, m.Login, []string{to}, body)
}

/*
	FakeMailer is a dummy doing nothing, for mocking
 */
type FakeMailer struct {
	SentTo      string
	SentSubject string
	SentBody    []byte
}

func (m *FakeMailer) SendEmail(to, subject string, body []byte) error {
	m.SentTo = to
	m.SentSubject = subject
	m.SentBody = body
	return nil
}
