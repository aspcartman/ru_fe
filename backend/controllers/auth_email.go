package controllers

import (
	"bitbucket.org/aspcartman/ru_fe/api"
	"github.com/goware/emailx"
	"github.com/pkg/errors"

	"context"
)

func (c AuthController) SignEmailUp(ctx context.Context, args *api.EmailCredentials) (*api.Token, error) {
	// Check args
	// Validate the email. Note the non-trivial way of validation
	if err := emailx.Validate(args.Email); err != nil {
		return nil, ErrBadEmail
	}
	// Validate the password
	if len(args.Password) == 0 {
		return nil, ErrBadPassword
	}

	// Do create the user in store
	id, err := c.CredentialsStore.CreateUserWithEmailAndPassword(args.Email, args.Password)
	if err != nil {
		return nil, err
	}

	// Return a token
	return c.TokenService.IssueToken(ctx, &api.TokenIssueArgs{UserId: id})
}

func (c AuthController) SignEmailIn(ctx context.Context, args *api.EmailCredentials) (*api.Token, error) {
	// Check args
	switch {
	case len(args.Email) == 0:
		return nil, errors.New("empty email")
	case len(args.Password) == 0:
		return nil, errors.New("empty password")
	}

	// Verify the password
	id, pass, err := c.CredentialsStore.GetUserIdAndPasswordByEmail(args.Email)
	switch {
	case err != nil:
		return nil, err
	case pass != args.Password:
		return nil, ErrPasswordMismatch
	}

	// Return a token
	return c.TokenService.IssueToken(ctx, &api.TokenIssueArgs{UserId: id})
}
