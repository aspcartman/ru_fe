package controllers

import (
	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
)

type AuthController struct {
	CredentialsStore   storage.UserCredentialsStore
	TokenService       api.TokenService
	GoogleCertificates Certs

	GoogleClientID string
	GoogleSecret   string
}
