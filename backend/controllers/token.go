package controllers

import (
	"context"
	"time"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
	"github.com/gbrlsnchs/jwt"
	"github.com/pkg/errors"
	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
)

/*
	AuthToken is responsible for validating and updating (if required) the session token.
	Token is an encrypted string, containing a json with a UserID of the token owner
	and the date, when the token was created.

	The fact of a successful decryption and the date actuality are the only factors of
	token validity for this test project.
 */

var signer = jwt.HS256("letsPretendItIsASafelyStoredKey")

const issuer = "rufe.TokenService"

type TokenController struct {
	Store storage.InvalidTokenStore
}

func (t TokenController) IssueToken(ctx context.Context, args *api.TokenIssueArgs) (*api.Token, error) {
	// Check args
	switch {
	case len(args.UserId) == 0:
		return nil, ErrNoUserID
	}

	// Create new token
	tok, err := jwt.Sign(signer, &jwt.Options{
		Audience:       args.UserId,
		ExpirationTime: time.Now().Add(1 * time.Hour),
		Issuer:         issuer,
		JWTID:          uuid.NewV4().String(),
		Timestamp:      true,
	})
	if err != nil {
		logrus.WithError(err).Error("failed generating token")
		return nil, err
	}

	return &api.Token{Token: tok}, nil
}

func (t TokenController) ValidateToken(ctx context.Context, args *api.Token) (*api.TokenValidationResult, error) {
	var userID string
	var invalidated bool

	// Parse
	tok, err := jwt.FromString(args.Token)
	if err != nil {
		goto validationFailure
	}

	// Check signature
	err = tok.Verify(signer)
	if err != nil {
		goto validationFailure
	}

	// Check all the other fields
	err = tok.Validate(
		jwt.ExpirationTimeValidator(time.Now()),
		jwt.IssuerValidator(issuer),
		jwt.NotBeforeValidator(time.Now()),
	)
	if err != nil {
		goto validationFailure
	}

	// Obtain UserID
	userID = tok.Audience()
	if userID == "" {
		err = ErrNoAudKey
		goto validationFailure
	}

	// We require tokens to contain jwtid to be able to invalidate them later
	if len(tok.ID()) == 0 {
		err = ErrNoJWTID
		goto validationFailure
	}

	// Finally, check if the token has been invalidated
	invalidated, err = t.Store.IsInvalidated(userID, tok.ID())
	if err != nil {
		goto validationFailure
	} else if invalidated {
		err = ErrTokenInvalid
		goto validationFailure
	}

	return &api.TokenValidationResult{Valid: true, TokenId: tok.ID(), UserId: userID}, nil

validationFailure:
	logrus.WithError(err).Warn("token validation failed")
	return &api.TokenValidationResult{InvalidyReason: err.Error()}, nil
}

func (t TokenController) InvalidateToken(ctx context.Context, args *api.Token) (*api.Empty, error) {
	// Check the token first. We don't want to invalidate a non-real token
	res, err := t.ValidateToken(ctx, args)
	switch {
	case err != nil:
		return nil, err
	case !res.Valid:
		return nil, errors.New(res.InvalidyReason)
	case len(res.UserId) == 0:
		return nil, ErrNoUserID
	case len(res.TokenId) == 0:
		return nil, ErrNoJWTID
	}

	// Finally save the token as invalid
	if err = t.Store.InvalidateToken(res.UserId, res.TokenId); err != nil {
		return nil, err
	}

	return &api.Empty{}, nil
}
