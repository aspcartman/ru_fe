package controllers

import (
	"context"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
	"bitbucket.org/aspcartman/ru_fe/backend/utility"
)

type PasswordRestoreController struct {
	CredentialsStore storage.UserCredentialsStore
	Mailer           utility.Mailer
}

func (c PasswordRestoreController) SendRestorePasswordEmail(ctx context.Context, args *api.SendRestorePasswordEmailArg) (*api.Empty, error) {
	if err := c.Mailer.SendEmail(args.Email, "RU_FE Password Restore", []byte("Hello =)")); err != nil {
		return nil, err
	}
	return &api.Empty{}, nil
}

func (c PasswordRestoreController) RestorePassword(ctx context.Context, args *api.RestorePasswordArgs) (*api.Token, error) {
	panic("implement me")
}
