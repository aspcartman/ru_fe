package controllers

import (
	"bitbucket.org/aspcartman/ru_fe/api"
	"context"
)

func (c AuthController) SignOut(ctx context.Context, arg *api.Token) (*api.Empty, error) {

	return c.TokenService.InvalidateToken(ctx, arg)
}
