package controllers

import "errors"

var (
	ErrNotAllowed       = errors.New("not allowed")
	ErrBadEmail         = errors.New("bad email")
	ErrBadPassword      = errors.New("bad password")
	ErrPasswordMismatch = errors.New("password mismatch")
	ErrNoToken          = errors.New("no token")
	ErrNoUserID         = errors.New("no userID")
	ErrTokenInvalid     = errors.New("token is invalid")
	ErrNoAudKey         = errors.New("no aud value")
	ErrNoJWTID          = errors.New("no jwtid value")
)
