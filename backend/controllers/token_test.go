package controllers_test

import (
	"context"
	"testing"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/controllers"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
)

func TestTokenController(t *testing.T) {
	ctx := context.Background()
	c := controllers.TokenController{Store: &storage.InMemStore{}}

	tok, err := c.IssueToken(ctx, &api.TokenIssueArgs{UserId: "123"})
	switch {
	case err != nil:
		t.Error(err)
		t.FailNow()
	case tok == nil:
		t.Error("tok == nil")
		t.FailNow()
	}

	val, err := c.ValidateToken(ctx, tok)
	switch {
	case err != nil:
		t.Error(err)
		t.FailNow()
	case !val.Valid:
		t.Error(val.InvalidyReason)
		t.FailNow()
	case val.UserId != "123":
		t.Error("invalid userid", val.UserId)
		t.FailNow()
	}

	_, err = c.InvalidateToken(ctx, tok)
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	val, err = c.ValidateToken(ctx, tok)
	switch {
	case err != nil:
		t.Error(err)
		t.FailNow()
	case val.Valid:
		t.Error("token is still valid after invalidation")
		t.FailNow()
	case val.InvalidyReason == "":
		t.Error("empty invalidy reason")
		t.FailNow()
	}

}
