package controllers

import (
	"context"
	"errors"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/model"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
	"bitbucket.org/aspcartman/ru_fe/common/tokenutil"
)

type ProfileController struct {
	TokenService api.TokenService
	Store        storage.ProfileStore
}

func (c ProfileController) GetUserProfile(ctx context.Context, args *api.GetUserProfileArgs) (*api.Profile, error) {
	// Args check
	switch {
	case len(args.UserId) == 0:
		return nil, errors.New("no user id")
	}

	// Users are not allowed to access data of other users
	_, authUserId := tokenutil.GetTokenFromContext(ctx)
	if authUserId != args.UserId {
		return nil, ErrNotAllowed
	}

	// Obtain data from source
	prof, err := c.Store.GetUserProfile(args.UserId)
	if err != nil {
		return nil, err
	}

	// Mapping
	return &api.Profile{
		Name:    prof.Name,
		Age:     int64(prof.Age),
		Address: prof.Address,
	}, nil
}

func (c ProfileController) ModifyUserProfile(ctx context.Context, args *api.ModifyUserProfileArgs) (*api.Empty, error) {
	// Args check
	switch {
	case len(args.UserId) == 0:
		return nil, ErrNoUserID
	case args.Profile == nil:
		return nil, errors.New("no profile")
	}


	// Permission check
	_, authUserId := tokenutil.GetTokenFromContext(ctx)
	if authUserId != args.UserId {
		return nil, ErrNotAllowed
	}

	// Do the update
	err := c.Store.UpdateUserProfile(args.UserId, &model.UserProfile{
		Name:    args.Profile.Name,
		Age:     int(args.Profile.Age),
		Phone:   args.Profile.Phone,
		Address: args.Profile.Address,
	})

	if err != nil {
		return nil, err
	}

	return &api.Empty{}, nil
}
