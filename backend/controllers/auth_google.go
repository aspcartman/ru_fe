package controllers

import (
	"context"
	"crypto/rsa"
	"encoding/base64"
	"encoding/json"
	"math/big"
	"net/http"
	"regexp"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/aspcartman/ru_fe/api"
	"bitbucket.org/aspcartman/ru_fe/backend/storage"
	"github.com/futurenda/google-auth-id-token-verifier"
	"github.com/goware/emailx"
	"github.com/sirupsen/logrus"
)

func (c AuthController) SignGoogle(ctx context.Context, args *api.GoogleCredentials) (*api.Token, error) {
	cl, err := googleAuthIDTokenVerifier.Decode(args.GoogleToken)
	if err != nil {
		return nil, err
	}

	// todo: verify token

	// If exists
	id, _, err := c.CredentialsStore.GetUserIdAndPasswordByEmail(cl.Email)
	switch {
	case err == storage.ErrNotFound:
	case err != nil:
		return nil, err
	default:
		return c.TokenService.IssueToken(ctx, &api.TokenIssueArgs{UserId: id})
	}

	// SignUp
	if err = emailx.Validate(cl.Email); err != nil {
		logrus.WithError(err).Warn("email validation failed")
		return nil, ErrBadEmail
	}

	id, err = c.CredentialsStore.CreateUserWithEmailAndPassword(cl.Email, "") // Empty passwords are safe since they are not allowed for signin
	if err != nil {
		return nil, err
	}

	return c.TokenService.IssueToken(ctx, &api.TokenIssueArgs{UserId: id})
}

type Certs struct {
	keys   map[string]*rsa.PublicKey
	expiry time.Time
	mtx    sync.Mutex
}

func (c *Certs) Get() (map[string]*rsa.PublicKey, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	if c.expiry.After(time.Now()) {
		return c.keys, nil
	}

	// Fetch keys from Google
	resp, err := http.Get("https://www.googleapis.com/oauth2/v3/certs")
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Determine the cache age
	cacheAge := int64(7200) // Set default cacheAge to 2 hours
	cacheControl := resp.Header.Get("cache-control")
	if len(cacheControl) > 0 {
		match := regexp.MustCompile("max-age=([0-9]*)").FindAllStringSubmatch(cacheControl, -1)
		if len(match) > 0 {
			if len(match[0]) == 2 {
				maxAge := match[0][1]
				maxAgeInt, err := strconv.ParseInt(maxAge, 10, 64)
				if err != nil {
					return nil, err
				}
				cacheAge = maxAgeInt
			}
		}
	}

	// Get keys out of the json
	type key struct {
		Kty string `json:"kty"`
		Alg string `json:"alg"`
		Use string `json:"use"`
		Kid string `json:"Kid"`
		N   string `json:"n"`
		E   string `json:"e"`
	}
	var body struct {
		keys []*key `json:"keys"`
	}
	err = json.NewDecoder(resp.Body).Decode(&body)
	if err != nil {
		return nil, err
	}

	// Put keys into a map
	keys := map[string]*rsa.PublicKey{}
	for _, key := range body.keys {
		if key.Use == "sig" && key.Kty == "RSA" {
			n, err := base64.RawURLEncoding.DecodeString(key.N)
			if err != nil {
				return nil, err
			}
			e, err := base64.RawURLEncoding.DecodeString(key.E)
			if err != nil {
				return nil, err
			}
			ei := big.NewInt(0).SetBytes(e).Int64()
			if err != nil {
				return nil, err
			}
			keys[key.Kid] = &rsa.PublicKey{
				N: big.NewInt(0).SetBytes(n),
				E: int(ei),
			}
		}
	}

	c.keys = keys
	c.expiry = time.Now().Add(time.Second * time.Duration(cacheAge))

	return keys, nil
}
