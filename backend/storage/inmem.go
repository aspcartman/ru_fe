package storage

import (
	"bitbucket.org/aspcartman/ru_fe/backend/model"
	"fmt"
)

/*
	InMemStore is a simple mocking store for tests.
	It stores all the objects inside one slice, which
	is very non performant, but simultaneously very convenient.
 */

type InMemStore []interface{}

func (m *InMemStore) GetUserProfile(id string) (*model.UserProfile, error) {
	return &model.UserProfile{}, nil
}

func (m *InMemStore) UpdateUserProfile(id string, profile *model.UserProfile) error {
	return nil
}

type invtoken struct {
	userid, tokenid string
}

func (m InMemStore) IsInvalidated(userid, tokenid string) (bool, error) {
	tok := invtoken{userid, tokenid}
	for _, v := range m {
		if t, ok := v.(invtoken); ok {
			if t == tok {
				return true, nil
			}
		}
	}
	return false, nil
}

func (m *InMemStore) InvalidateToken(userid, tokenid string) error {
	*m = append(*m, invtoken{userid, tokenid})
	return nil
}

func (m *InMemStore) CreateUserWithEmailAndPassword(email, password string) (string, error) {
	if _, _, err := m.GetUserIdAndPasswordByEmail(email); err != ErrNotFound {
		return "", ErrExist
	}

	id := len(*m)
	*m = append(*m, model.User{
		ID:       fmt.Sprint(id),
		Email:    email,
		Password: password,
	})

	return fmt.Sprint(id), nil
}

func (m InMemStore) GetUserIdAndPasswordByEmail(email string) (id, password string, err error) {
	for _, obj := range m {
		if u, ok := obj.(model.User); ok {
			if u.Email == email {
				return u.ID, u.Password, nil
			}
		}
	}
	return "", "", ErrNotFound
}
