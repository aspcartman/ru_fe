package storage

import "testing"

func TestMySQL(t *testing.T) {
	m, err := NewMysql("task", "task", "task")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	testUserCredentialStore(m, t)
}
