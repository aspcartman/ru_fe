package storage

import (
	"bitbucket.org/aspcartman/ru_fe/backend/model"
	"errors"
)

var ErrNotFound = errors.New("not found")
var ErrExist = errors.New("exists")

type UserCredentialsStore interface {
	CreateUserWithEmailAndPassword(email, password string) (string, error)
	GetUserIdAndPasswordByEmail(email string) (id, password string, err error)
}

type ProfileStore interface {
	GetUserProfile(id string) (*model.UserProfile, error)
	UpdateUserProfile(id string, profile *model.UserProfile) error
}

type InvalidTokenStore interface {
	IsInvalidated(userid, tokenid string) (bool, error)
	InvalidateToken(userid, tokenid string) error
}
