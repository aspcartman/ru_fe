package storage

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
)

type MySQLStore struct {
	db *sql.DB
}

func NewMysql(user, password, database string) (MySQLStore, error) {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@/%s?charset=utf8", user, password, database))
	if err != nil {
		return MySQLStore{}, err
	}

	return MySQLStore{db}, nil
}

func (m MySQLStore) CreateUserWithEmailAndPassword(email, password string) (string, error) {
	res, err := m.db.Exec("INSERT INTO users (email, password) VALUES (?,?)", email, password)
	if err != nil {
		return "", maperr(err)
	}

	last, err := res.LastInsertId()
	if err != nil {
		return "", maperr(err)
	}

	return fmt.Sprint(last), nil
}

func (m MySQLStore) GetUserIdAndPasswordByEmail(email string) (string, string, error) {
	var id int
	var pass string

	if err := m.db.QueryRow("SELECT id, password FROM users WHERE email = ?", email).Scan(&id, &pass); err != nil {
		return "", "", maperr(err)
	}

	return fmt.Sprint(id), pass, nil
}

func maperr(err error) error {
	if err == sql.ErrNoRows {
		return ErrNotFound
	}

	if dr, ok := err.(*mysql.MySQLError); ok {
		if dr.Number == 1062 {
			return ErrExist
		}
	}

	return err
}
