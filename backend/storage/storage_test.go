package storage

import "testing"

func testUserCredentialStore(s UserCredentialsStore, t *testing.T) {
	_, _, err := s.GetUserIdAndPasswordByEmail("test@email")
	if err != ErrNotFound {
		t.Error("found nonexisting user")
	}

	id, err := s.CreateUserWithEmailAndPassword("test@email", "somepass")
	switch {
	case err != nil:
		t.Errorf("can't create: %s", err.Error())
	case len(id) == 0:
		t.Error("empty id after insertion")
	}

	gid, pass, err := s.GetUserIdAndPasswordByEmail("test@email")
	switch {
	case err != nil:
		t.Errorf("can't get user back: %s", err.Error())
	case pass != "somepass":
		t.Error("wrong pass")
	case gid != id:
		t.Errorf("wrong id %s != %s", gid, id)
	}

	_, err = s.CreateUserWithEmailAndPassword("test@email", "somepass")
	switch err {
	case ErrExist:
	case nil:
		t.Error("didn't error on creating existing user")
	default:
		t.Errorf("wrong error %s", err.Error())
	}

	nid, err := s.CreateUserWithEmailAndPassword("other@email", "otherpass")
	switch {
	case err != nil:
		t.Errorf("can't create 2: %s", err.Error())
	case id == nid:
		t.Error("same id as before")
	case len(nid) == 0:
		t.Error("empty id on second insertion")
	}
}
