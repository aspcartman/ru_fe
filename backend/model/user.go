package model

type User struct {
	ID       string
	Email    string
	Password string
}

type UserProfile struct {
	Name    string
	Age     int
	Phone   string
	Address string
}
